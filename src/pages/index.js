import { useState, useEffect } from "react";
import { MainGrid } from "../components/MainGrid";
import { Box } from "../components/Box";
import { ProfileRelationBox } from "../components/ProfileRelationBox";

import {
    AlurakutMenu,
    AlurakutProfileSidebarMenuDefault,
    OrkutNostalgicIconSet,
} from "../lib/AlurakutCommons";

function ProfileSidebar(props) {
    return (
        <Box as="aside">
            <img
                style={{ borderRadius: "8px" }}
                src={`https://github.com/${props.github_user}.png`}
            />
            <hr />

            <p>
                <a
                    href={`https://github.com/${props.github_user}.png`}
                    className="boxLink"
                >
                    @{props.github_user}
                </a>
            </p>

            <hr />

            <AlurakutProfileSidebarMenuDefault />
        </Box>
    );
}

export default function Home() {
    const user_name = "eduardo-developer01";

    const [communities, setCommunites] = useState([
        {
            id: `f00`,
            title: "Eu gosto de acordar cedo",
            image: "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse3.explicit.bing.net%2Fth%3Fid%3DOIP.deUuZLdUfI-5zeEaxiNCggAAAA%26pid%3DApi&f=1",
        },
    ]);

    const community_weights = [
        "eduardo-developer01",
        "omariosouto",
        "juunegreiros",
        "marcobrunodev",
        "filipedeschamps",
        "maykbrito",
    ];
    const [followings, setFollowings] = useState([])

    useEffect(function () {
        fetch(`https://api.github.com/users/${user_name}/following`)
            .then((resServer) => {
                return resServer.json();
            })
            .then((resComp) => {
                setFollowings(resComp);
            });
    }, []);

    return (
        <>
            <AlurakutMenu githubUser={user_name} />
            <MainGrid>
                <div
                    className="profileArea"
                    style={{ gridArea: "profileArea" }}
                >
                    <ProfileSidebar github_user={user_name} />
                </div>
                <div
                    className="welcomeArea"
                    style={{ gridArea: "welcomeArea" }}
                >
                    <Box>
                        <h1 className="title">Bem vindos(a)</h1>
                        <OrkutNostalgicIconSet />
                    </Box>

                    <Box>
                        <h2 className="subTitle">O que Você deseja fazer?</h2>
                        <form
                            onSubmit={function handleCreateCommunit(e) {
                                e.preventDefault();

                                const formData = new FormData(e.target);
                                const communite = {
                                    id: new Date().toISOString(),
                                    title: formData.get("title"),
                                    image: formData.get("image"),
                                };

                                const newCommunites = [
                                    ...communities,
                                    communite,
                                ];
                                setCommunites(newCommunites);
                            }}
                        >
                            <div>
                                <input
                                    placeholder="Qual vai ser o nome da comunidade?"
                                    name="title"
                                    aria-label="Qual vai ser o nome da comunidade?"
                                    type="text"
                                />
                            </div>
                            <div>
                                <input
                                    placeholder="Coloque uma URL para usarmos de capa"
                                    name="image"
                                    aria-label="Coloque uma URL para usarmos de capa"
                                />
                            </div>

                            <button>Criar Comunidade</button>
                        </form>
                    </Box>
                </div>
                <div
                    className="profileRelationsArea"
                    style={{ gridArea: "profileRelationsArea" }}
                >
                    <ProfileRelationBox 
                        title="seguido" 
                        items={followings} 
                    />
                    <ProfileRelationBox
                        title="Perssoas da Comunidade"
                        items={community_weights}
                    />
                    <ProfileRelationBox
                        title="Comunidades"
                        items={communities}
                    />
                </div>
            </MainGrid>
        </>
    );
}
