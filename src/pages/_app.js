import { createGlobalStyle } from "styled-components";
import { AlurakutStyles } from "../lib/AlurakutCommons";

const GlobalStyle = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }

  body {
    background-image: url(https://images.pexels.com/photos/3293148/pexels-photo-3293148.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260);
    background-size: 100%;
    background-position: bottom;
    font-family: sans-serif;
  }

  #__next {
    display: flex;
    min-height: 100vh;
    flex-direction: column;
  }

  img {
    max-width: 100%;
    height: auto;
    display: block;
  }

  ${AlurakutStyles}
`;


export default function App({ Component, pageProps }) {
	return (
		<>
      <GlobalStyle />
      <Component {...pageProps} />
		</>
	);
}
