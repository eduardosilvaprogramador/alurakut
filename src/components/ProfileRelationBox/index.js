import { Container } from "./style"

export function ProfileRelationBox(props) {
    return (
        <Container>
            <h2 className="smallTitle">
                {props.title} ({props.items.length})
            </h2>
        
            <ul>
                {props.items.map((item_atual) => {
                    return (
                        <li key={item_atual} >
                            <a
                                href={`/users/${item_atual.title}`}
                                key={item_atual.title}
                            >
                                <img src={item_atual.image} />
                                <span>{item_atual.title}</span>
                            </a>
                        </li>
                    );
                })}
            </ul>
        </Container>
    )
}

export const BoxFollowings = (props) => {
    <Container>
        <h2 className="smallTitle">
            {props.title} ({props.items.length})
        </h2>

        <ul>
            {props.items.map((item_atual) => {
                return (
                    <li key={item_atual}>
                        <a href={`/users/${item_atual}`} key={item_atual}>
                            <img src={`https://github.com/${item_atual}.png`} />
                            <span>{item_atual}</span>
                        </a>
                    </li>
                );
            })}
        </ul>
    </Container>
}

